﻿namespace Vote.Constants
{
    public static class PermissionPolicy
    {
        public const string Manage = "ManageArea";

        public static string[] All()
        {
            return new[]
            {
                Manage,
            };
        }
    }
}
