﻿using System;
using Microsoft.Extensions.Logging;

namespace Vote.Support
{
    public static class LoggerExtensions
    {
        private static readonly Action<ILogger, string, Exception?> LogTraceDelegate = LoggerMessage.Define<string>(
                   LogLevel.Trace,
                   new EventId(0, "msg"),
                   "{Message}");

        private static readonly Action<ILogger, string, Exception?> LogDebugDelegate = LoggerMessage.Define<string>(
                    LogLevel.Debug,
                    new EventId(1, "msg"),
                    "{Message}");

        private static readonly Action<ILogger, string, Exception?> LogInformationDelegate = LoggerMessage.Define<string>(
                    LogLevel.Information,
                    new EventId(2, "msg"),
                    "{Message}");

        private static readonly Action<ILogger, string, Exception?> LogWarningDelegate = LoggerMessage.Define<string>(
                    LogLevel.Warning,
                    new EventId(3, "msg"),
                    "{Message}");

        private static readonly Action<ILogger, string, Exception?> LogErrorDelegate = LoggerMessage.Define<string>(
                    LogLevel.Error,
                    new EventId(4, "msg"),
                    "{Message}");

        private static readonly Action<ILogger, string, Exception?> LogCriticalDelegate = LoggerMessage.Define<string>(
                    LogLevel.Critical,
                    new EventId(5, "default"),
                    "{Message}");

        public static void LogTrace(this ILogger logger, string message)
        {
            LogTraceDelegate(logger, message, null);
        }

        public static void LogDebug(this ILogger logger, string message)
        {
            LogDebugDelegate(logger, message, null);
        }

        public static void LogInformation(this ILogger logger, string message)
        {
            LogInformationDelegate(logger, message, null);
        }

        public static void LogWarning(this ILogger logger, string message)
        {
            LogWarningDelegate(logger, message, null);
        }

        public static void LogError(this ILogger logger, string message)
        {
            LogErrorDelegate(logger, message, null);
        }

        public static void LogCritical(this ILogger logger, string message)
        {
            LogCriticalDelegate(logger, message, null);
        }
    }
}
