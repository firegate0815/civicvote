﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using VoteDatabase.DataAccess;
using VoteDatabase.Models.BO;

namespace Vote.Support
{
    public class WebRequestWriter
    {
        private readonly RequestDelegate mNext;

        public WebRequestWriter(RequestDelegate next)
        {
            mNext = next;
        }

        public async Task InvokeAsync(
            HttpContext httpContext,
            VoteDbContext dbContext,
            Microsoft.Extensions.Logging.ILogger<WebRequestWriter> logger,
            Microsoft.Extensions.Options.IOptions<Models.Settings.VoteSettings> settings)
        {
            if (dbContext == null) { throw new ArgumentNullException(nameof(dbContext)); }
            if (httpContext == null) { throw new ArgumentNullException(nameof(httpContext)); }

            try
            {
                if (settings?.Value.EnableRequestLogging != true) { return; }

                var path = httpContext.Request.Path.Value;
                if (path?.TakeLast(6).Contains('.') == true) { return; }

                var identity = httpContext.User?.Identity?.Name;

                var request = new WebRequest(
                    timestamp: DateTime.UtcNow,
                    identity: identity,
                    remoteIpAddress: httpContext.Connection.RemoteIpAddress,
                    method: httpContext.Request.Method,
                    userAgent: httpContext.Request.Headers["User-Agent"],
                    referer: httpContext.Request.Headers["Referrer"],
                    path: path,
                    query: httpContext.Request.QueryString.Value,
                    isWebSocket: httpContext.WebSockets.IsWebSocketRequest);

                dbContext.WebRequest.Add(request);
                await dbContext.SaveChangesAsync().ConfigureAwait(false);
            }
#pragma warning disable CA1031 // Do not catch general exception types
            catch (Exception ex)
#pragma warning restore CA1031 // Do not catch general exception types
            {
                logger.LogError($"{nameof(WebRequestWriter)} failed to store request: {ex}");
            }
            finally
            {
                await mNext(httpContext).ConfigureAwait(false);
            }
        }
    }
}
