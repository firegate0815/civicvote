﻿namespace Vote.Areas
{
    public static class UrlPaths
    {
        #region Legal area
        public const string Legal = nameof(Areas.Legal);
        public static string Datenschutz { get; } = $"/{Legal}/{nameof(Areas.Legal.Pages.DatenschutzModel)[..^5]}";
        public static string Impressum { get; } = $"/{Legal}/{nameof(Areas.Legal.Pages.ImpressumModel)[..^5]}";
        #endregion

        #region State area
        public const string State = nameof(Areas.State);
        public static string Error { get; } = $"/{Legal}/{nameof(Areas.State.Pages.ErrorModel)[..^5]}";
        public static string Heartbeat { get; } = $"/{Legal}/{nameof(Areas.State.Pages.HeartbeatModel)[..^5]}";
        #endregion

        #region Vote area
        public const string Vote = nameof(Areas.Vote);
        public static string Create { get; } = $"{Legal}/{nameof(Areas.Vote.Pages.CreateModel)[..^5]}";
        public static string Browse { get; } = $"{Legal}/{nameof(Areas.Vote.Pages.BrowseModel)[..^5]}";
        #endregion

        #region Identity area
        public const string Identity = nameof(Areas.Identity);
        public static string Login { get; } = $"{Identity}/{nameof(Areas.Identity.Pages.Account)}/{nameof(Areas.Identity.Pages.Account.LoginModel)[..^5]}";
        #endregion
    }
}
