using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Vote.Areas.Manage.Pages.InstitutionType
{
    public class IndexModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public IndexModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionType = null!;
        }

#pragma warning disable CA2227 // Collection properties should be read only
        public IList<VoteDatabase.Models.BO.InstitutionType> InstitutionType { get; set; }
#pragma warning restore CA2227 // Collection properties should be read only

        public async Task OnGetAsync()
        {
            InstitutionType = await mContext.InstitutionType
                .Include(i => i.Branch).ToListAsync().ConfigureAwait(false);
        }
    }
}
