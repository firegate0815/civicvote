﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Vote.Areas.Manage.Pages.User
{
    public class DetailsModel : PageModel
    {
        private readonly UserManager<VoteDatabase.Models.BO.User> mUserManager;

        public DetailsModel(UserManager<VoteDatabase.Models.BO.User> userManager)
        {
            mUserManager = userManager;
            VoteUser = null!;
        }

        public VoteDatabase.Models.BO.User VoteUser { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            VoteUser = await mUserManager.FindByIdAsync(id).ConfigureAwait(false);

            if (VoteUser == null)
            {
                return NotFound();
            }

            VoteUser.Roles = await mUserManager.GetRolesAsync(VoteUser).ConfigureAwait(false);

            return Page();
        }
    }
}
