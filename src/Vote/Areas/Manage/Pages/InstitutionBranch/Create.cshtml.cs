﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Vote.Areas.Manage.Pages.InstitutionBranch
{
    public class CreateModel : PageModel
    {
        private readonly VoteDatabase.DataAccess.VoteDbContext mContext;

        public CreateModel(VoteDatabase.DataAccess.VoteDbContext context)
        {
            mContext = context;
            InstitutionBranch = null!;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public VoteDatabase.Models.BO.InstitutionBranch InstitutionBranch { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            mContext.InstitutionBranch.Add(InstitutionBranch);
            await mContext.SaveChangesAsync().ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
    }
}
