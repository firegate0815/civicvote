using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Vote.Areas.Manage.Pages.Role
{
    public class DeleteModel : PageModel
    {
        private readonly RoleManager<IdentityRole> mRoleManager;

        public DeleteModel(RoleManager<IdentityRole> roleManager)
        {
            Role = null!;
            mRoleManager = roleManager;
        }

        [BindProperty]
        public IdentityRole Role { get; set; }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Role = await mRoleManager.FindByIdAsync(id).ConfigureAwait(false);

            return Role == null ? NotFound() : Page();
        }

        public async Task<IActionResult> OnPostAsync(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Role = await mRoleManager.FindByIdAsync(id).ConfigureAwait(false);

            if (Role != null)
            {
                await mRoleManager.DeleteAsync(Role).ConfigureAwait(false);
            }

            return RedirectToPage("./Index");
        }
    }
}
