using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using VoteDatabase.DataAccess;

namespace Vote.Areas.Manage.Pages.Role
{
    public class CreateModel : PageModel
    {
        private readonly VoteDbContext mContext;
        private readonly RoleManager<IdentityRole> mRoleManager;

        public CreateModel(VoteDbContext context, RoleManager<IdentityRole> roleManager)
        {
            mContext = context;
            mRoleManager = roleManager;
            Role = null!;
        }

        public IActionResult OnGet()
        {
            //ViewData["FkRoleType"] = new SelectList(mContext.RoleType, "Id", "Name");
            return Page();
        }

        [BindProperty]
        public IdentityRole<string> Role { get; set; }

        // To protect from over-posting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            await mRoleManager.CreateAsync(new IdentityRole(Role.Name)).ConfigureAwait(false);
            await mContext.SaveChangesAsync().ConfigureAwait(false);

            return RedirectToPage("./Index");
        }
    }
}
