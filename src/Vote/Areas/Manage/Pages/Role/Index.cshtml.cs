using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;

namespace Vote.Areas.Manage.Pages.Role
{
    public class IndexModel : PageModel
    {
        private readonly RoleManager<IdentityRole> mRoleManager;

        public IndexModel(RoleManager<IdentityRole> roleManager)
        {
            mRoleManager = roleManager;
            Role = null!;
        }

        public IList<IdentityRole> Role { get; private set; }

        public async Task OnGetAsync()
        {
            Role = await mRoleManager.Roles.ToListAsync().ConfigureAwait(false);
        }
    }
}
