﻿using Piranha.Extend;
using Piranha.Extend.Fields;

namespace Vote.Models.Cms.Blocks
{
    [BlockType(Name = "Button", Category = "Html forms", Icon = "fas fa-mouse-pointer", IsGeneric = true)]
    public class ButtonBlock : Block
    {
        /// <summary>
        /// Make button a submit button?
        /// </summary>
        [Field(Title = "Submit button?", Options = Piranha.Models.FieldOption.HalfWidth)]
        public CheckBoxField IsSubmit { get; set; } = null!;

        /// <summary>
        /// Name of upload element
        /// </summary>
        [Field(Title = "Button title", Options = Piranha.Models.FieldOption.HalfWidth)]
        public StringField ButtonTitle { get; set; } = null!;
    }
}
