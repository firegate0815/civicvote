﻿using System.ComponentModel.DataAnnotations;
using Piranha.Extend;
using Piranha.Extend.Fields;

namespace Vote.Models.Cms.Blocks
{
    [BlockType(Name = "Upload", Category = "Html forms", Icon = "fas fa-upload", IsGeneric = true)]
    public class UploadBlock : Block
    {
        /// <summary>
        /// HTML ID of upload element
        /// </summary>
        [Field(Options = Piranha.Models.FieldOption.HalfWidth)]
        [Required]
        public StringField UploadId { get; set; } = null!;

        /// <summary>
        /// Name of upload element
        /// </summary>
        [Field(Options = Piranha.Models.FieldOption.HalfWidth)]
        [Required]
        public StringField UploadName { get; set; } = null!;

        /// <summary>
        /// Accept upload file MIME types
        /// </summary>
        [Field(Options = Piranha.Models.FieldOption.HalfWidth, Placeholder = "image/png, image/jpeg")]
        [Required]
        public StringField AcceptTypes { get; set; } = null!;
    }
}
