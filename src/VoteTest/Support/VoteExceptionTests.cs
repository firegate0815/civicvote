﻿using System;
using AutoFixture;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Vote.Support;

namespace VoteTest.Support
{
    [TestClass]
    public class VoteExceptionTests
    {
        [TestMethod]
        public void VoteException_ctor()
        {
            Fixture fixture = new Fixture();
            var message = fixture.Create<string>();
            var innerException = fixture.Create<Exception>();

            var sut = new VoteException(message, innerException);
            Assert.AreEqual(message, sut.Message);
            Assert.AreEqual(innerException, sut.InnerException);

            sut = new VoteException(message);
            Assert.AreEqual(message, sut.Message);
            Assert.AreEqual(null, sut.InnerException);

            sut = new VoteException();
            Assert.AreEqual("Exception of type 'Vote.Support.VoteException' was thrown.", sut.Message);
            Assert.AreEqual(null, sut.InnerException);
        }
    }
}
