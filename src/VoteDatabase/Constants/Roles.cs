﻿namespace VoteDatabase.Constants
{
    public static class Roles
    {
        public const string Administrator = "Administrator";
        public const string PiranhaAdministrator = "PiranhaAdministrator";
    }
}
