﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Identity;

namespace VoteDatabase.Models.BO
{
    public class User : IdentityUser, IEquatable<User>
    {
        public string? CustomTag2 { get; set; }

        [NotMapped]
#pragma warning disable CA2227 // Collection properties should be read only
        public IList<string>? Roles { get; set; }
#pragma warning restore CA2227 // Collection properties should be read only

        public bool Equals(User? other)
        {
            return Id == other?.Id;
        }

        public override bool Equals(object? obj)
        {
            return Equals(obj as User);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode(StringComparison.InvariantCulture);
        }
    }
}
