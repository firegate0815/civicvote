## Code Snippets

These code snippets are for Visual Studio. Install them by copying them to

    %USERPROFILE%\Documents\Visual Studio 2022\Code Snippets\

Use them via auto-completion. E.g. type "di". This brings up the auto-completion dialog and suggests "disable_for_testing". Press TAB twice to use the code snippet.