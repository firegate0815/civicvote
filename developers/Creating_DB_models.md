
First create DataModel, e.g.

    public class BCase
    {
        [Key]
        public uint BCaseId { get; set; }
        public DateTime CreationDate { get; set; }
    }

Then right click "Controllers", add Skeleton for MVC Controller with views using EF.

Finally create migration script:

    add-migration -Context VoteDbContext InitialMigration

Then update database:

    update-database
    
Revert:

    update-database -Context VoteDbContext <PreviousMigration>
    Remove-Migration -Context VoteDbContext

 
|PMC Command	|Usage|
--- | ---
|Get-Help EntityFramework |Displays information about entity framework commands.|
|Add-Migration <migration name> |Creates a migration by adding a migration snapshot.|
|Remove-Migration |Removes the last migration snapshot.|
|Update-Database |Updates the database schema based on the last migration snapshot.|
|Script-Migration |Generates a SQL script using all the migration snapshots.|
|Scaffold-DbContext |Generates a DbContext and entity type classes for a specified database. This is called reverse engineering.|
|Get-DbContext |Gets information about a DbContext type.|
|Drop-Database |Drops the database.