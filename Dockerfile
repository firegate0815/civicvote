###
### base image to build the Dotnet application
### We add `npm` here as it is needed by the Vote project
###
FROM mcr.microsoft.com/dotnet/sdk:5.0 AS base-build
RUN apt-get update && apt-get install -y --no-install-recommends npm

###
### image used to build all projects
###
FROM base-build AS build
WORKDIR /src

COPY src .
COPY ./.editorconfig .

FROM build AS publish
RUN dotnet publish -c Release -o /app

FROM mcr.microsoft.com/dotnet/aspnet:5.0 AS base
WORKDIR /app
EXPOSE 80

###
### build the final image containing the application only
###
FROM base AS final
WORKDIR /app

COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Vote.dll"]
