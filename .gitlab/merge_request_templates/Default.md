<!-- Thank you for contributing to the project! Your work is highly appreciated.

     The content of the merge request is used to generate the release
     documentation and is also used as the commit description for the
     `master`commit. -->

<!-- title: must follow the conventional commit specification (enforced by the pipeline)
            - one sentence only (without a dot at the end)
            - don't capitalize the first letter
            - use imperative, e.g. "change" instead of "changed"

            Prefixes used:
              - feat: adds new functionality
              - fix: fixes a bug, something which is broken
              - refactor: does not fix a bug nor adds a new feature and no change from below
              - perf: improves the performance only
              - test: adds tests only
              - docs: adds documentation only
              - style: leaves the meaning of the code unchanged but corrects formatting, whitespaces, ...
              - build: changes the build system
              - ci: changes the CI scripts
              - chore: necessary work like dependency updates or changes to `.gitignore`

              examples:
                - feat: add the save file button
                - chore: bump log4j dependency to 2.17.1
                - docs: amend documentation for "register new user" use case
                - style: remove empty lines in package abc
-->

# Description
<!-- Please describe what you did and why you created the merge request, e.g. which
     feature do you implement, which bug you fix, ... Anything which is helpful
     for the reviewer to get the context. -->

# Test Plan
<!-- Describe how to test your change, what you did to test it. Remember: Automatic
     tests are necessary and preferred. -->

# Issues
<!-- Mention the issues, merge requests, ... you are referring to. Use `Closes #1`
     to automatically close the referenced ticket as soon as the merge request
     is merged. -->

# Checklist
- [ ] Automatic tests added
- [ ] Documentation amended
- [ ] Issues for follow ups/further enhancements created
